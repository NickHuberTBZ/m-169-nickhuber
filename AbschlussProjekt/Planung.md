### **Projektplanung: WordPress mit Docker, MariaDB und Portainer auf Windows**

#### **Projektübersicht**
Dieses Projekt zielt darauf ab, eine containerisierte WordPress-Website auf Windows mit Docker zu erstellen. Die Website wird eine MySQL-Datenbank als Backend nutzen und sowohl Docker-Compose als auch Docker Desktop einsetzen. Zudem soll Portainer als Monitoring-Tool eingesetzt werden.

![dockerxwordpress](images/wordpressXdocker.png)

#### **Ziele und Anforderungen**
- **Hauptziel:** WordPress-Website in einer containerisierten Umgebung auf Windows bereitstellen.
- **Anforderungen:**
  - Verwendung von Docker Desktop für die Container-Verwaltung.
  - WordPress-Container mit dem offiziellen WordPress-Image.
  - MySQL-Datenbank in einem separaten Container.
  - Docker-Compose zur Orchestrierung der Container.
  - Portainer für die Container-Überwachung und -Verwaltung.

#### **Zeitplan**
- **Woche 1: Planung & Vorbereitungen**
  - Anforderungen definieren und Projektplanung erstellen.
  - Docker Desktop installieren und einrichten.

- **Woche 2: Setup & Entwicklung**
  - Docker-Compose-Datei erstellen und konfigurieren.
  - MariaDB-Datenbank und WordPress-Container aufsetzen.
  - WordPress installieren und konfigurieren.

- **Woche 3: Testen, Feinschliff, Monitoring & Dokumentation**
  - Portainer als Monitoring-Tool aufsetzen.
  - Testen der WordPress-Website.
  - Fehlerbehebung und Optimierung.
  - Endgültige Bereitstellung und Übergabe.
  - Dokumentation des Projekts.

#### **Schritte im Detail**

**Woche 1: Planung & Vorbereitungen**
1. **Anforderungen definieren:**
   - Identifizieren der wichtigsten Anforderungen.
   - Definieren der benötigten Komponenten.

2. **Docker Desktop installieren und einrichten:**
   - Docker Desktop von der [offiziellen Website](https://www.docker.com/products/docker-desktop) herunterladen.
   - Installation und Einrichtung mit WSL2-Unterstützung.

**Woche 2: Setup & Entwicklung**
1. **Docker-Compose-Datei erstellen:**
   - `docker-compose.yml` mit folgenden Services erstellen:
     - MySQL-Datenbank (`mysql:debian` Image)
     - WordPress (`wordpress:latest` Image)

2. **Datenbank-Container starten:**
   - Starten des MariaDB-Containers mit den Umgebungsvariablen:
     - `MYSQL_ROOT_PASSWORD`
     - `MYSQL_DATABASE`
     - `MYSQL_USER`
     - `MYSQL_PASSWORD`

3. **WordPress-Container starten:**
   - Konfigurieren und Starten des WordPress-Containers mit:
     - Datenbank-Host `db`
     - Datenbank-Benutzername, Passwort und Name

4. **WordPress konfigurieren:**
   - Datenbankverbindungsdetails in WordPress eintragen.
   - WordPress-Installation abschließen.

**Woche 3: Testen, Feinschliff, Monitoring & Dokumentation**
1. **Portainer als Monitoring-Tool aufsetzen:**
   - Docker-Volume für Portainer erstellen:
     ```bash
     docker volume create portainer_data
     ```
   - Portainer-Container starten:
     ```bash
     docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
     ```
   - Portainer im Browser unter `http://localhost:9000` öffnen und einrichten.

2. **Website testen:**
   - Testen der Website-Funktionen.
   - Überprüfen, ob alle Komponenten korrekt funktionieren.

3. **Fehlerbehebung und Optimierung:**
   - Fehler identifizieren und beheben.
   - Docker-Compose-Datei optimieren, falls nötig.

4. **Dokumentation:**
   - Erstellen einer umfassenden Dokumentation über das Projekt.
   - Eine Anleitung zur Nutzung und Wartung bereitstellen.