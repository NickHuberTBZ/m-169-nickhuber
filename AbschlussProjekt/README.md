# **Dokumentation M169**

![dockerxwordpress](images/wordpressXdocker.png)

## **Projekte mit Docker und WordPress**
1. [**Teil 1: Docker & WordPress kennenlernen**](#Teil-1-docker--wordpress-kennenlernen)
    - [**Docker installieren**](#docker-installieren)
    - [**WordPress-Container erstellen und konfigurieren**](#wordpress-container-erstellen-und-konfigurieren)
2. [**Teil 2: WordPress mit Docker aufsetzen**](#Teil-2-wordpress-mit-docker-aufsetzen)
    - [**Docker-Setup**](#docker-setup)
    - [**PhpMyAdmin**](#phpmyadmin)
    - [**Monitoring der Container**](#monitoring-der-container)
    - [**Sicherheit im Container-Umfeld**](#sicherheit-im-container-umfeld)

- [**Fertige Website**](#FertigeWebsite)

## **Projektziele**
Das Ziel dieser Projekte war es, eine WordPress-Webseite in einer containerisierten Umgebung mithilfe von Docker bereitzustellen.

## **Teil 1: Docker & WordPress**
### **Docker installieren**
Ich habe Docker auf Windows gemäß den offiziellen [Anweisungen](https://docs.docker.com/desktop/install/windows-install/) installiert.

1. Docker Desktop für Windows herunterladen und installieren.
   - [Docker Desktop Download](https://www.docker.com/products/docker-desktop/)
2. Docker Desktop starten und den Anweisungen für die Erstkonfiguration folgen.
3. Überprüfen, dass Docker korrekt installiert ist:
   ```bash
   docker --version
   ```

### **WordPress-Container erstellen und konfigurieren**
1. MariaDB-Image herunterladen:
   ```bash
   docker pull mariadb
   ```
   > WordPress benötigt eine Datenbank. Daher habe ich zuerst einen MariaDB-Container erstellt.

2. MariaDB-Container starten:
   ```bash
   docker run --name my-db -e MYSQL_ROOT_PASSWORD=db-password -d mariadb
   ```
3. WordPress-Image herunterladen:
   ```bash
   docker pull wordpress
   ```
4. WordPress-Container starten:
   ```bash
   docker run --name my-wordpress -p 8080:80 --link my-db:mariadb -d wordpress
   ```
   > Der Container wird auf Port 8080 gestartet und mit der MariaDB-Datenbank verbunden.

5. Datenbank für WordPress einrichten:
   ```sql
   CREATE DATABASE wordpress DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
   CREATE USER 'wp_user'@'localhost' IDENTIFIED BY 'tbz2024';
   GRANT ALL ON wordpress.* TO 'wp_user'@'localhost';
   ```
6. Einstellungen auf Gui
   Ich habe mich dann als nächtes auf dem localhost angemeldet und dort Einstellungen gemacht.

   ![wordpressLogin](images/wordpress-login.png)
   ![wordpressLogin](images/succesful-login-wordpress.png)

## **Teil 2: WordPress mit Docker aufsetzen**
### **Docker-Setup**
1. Docker-Compose-Datei erstellen:
   - In einem neuen Verzeichnis eine Datei `docker-compose.yml` erstellen und den folgenden Inhalt hinzufügen:
     ```yaml
     version: '3.1'
     
     services:
       wordpress:
         image: wordpress
         restart: always
         ports:
           - 8080:80
         environment:
           WORDPRESS_DB_HOST: db
           WORDPRESS_DB_USER: wp_user
           WORDPRESS_DB_PASSWORD: tbz2024
           WORDPRESS_DB_NAME: wordpress
       db:
         image: mariadb
         restart: always
         environment:
           MYSQL_ROOT_PASSWORD: db-password
           MYSQL_DATABASE: wordpress
           MYSQL_USER: wp_user
           MYSQL_PASSWORD: tbz2024
     ```
2. Docker-Compose-Datei ausführen:
   ```bash
   docker compose up -d
   ```

   ![dockerCompose](images/bootingUp-dockerFile.png)

### **PhpMyAdmin**
Ich wollte PhpMyAdmin zur Verwaltung von MySQL nutzen, entschied mich aber aufgrund von Kompatibilitätsproblemen dagegen. Stattdessen entschied ich mich für die Verwaltung über Docker.

### **Monitoring mit Portainer**

Als Teil des Projekts wurde Portainer als Monitoring- und Verwaltungswerkzeug für die Docker-Container gewählt. Portainer bietet eine benutzerfreundliche Web-Oberfläche, die eine einfache Überwachung und Verwaltung der Container ermöglicht. Hier sind die Schritte zur Integration:

1. **Einrichtung von Portainer:**
   - Zunächst wurde ein Docker-Volume für Portainer erstellt, um sicherzustellen, dass die Daten von Portainer persistent gespeichert werden:
     ```bash
     docker volume create portainer_data
     ```
   - Anschließend wurde der Portainer-Container mit den entsprechenden Bind-Mounts und Netzwerk-Ports gestartet:
     ```bash
     docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
     ```
   - Portainer wurde dann auf `http://localhost:9000` im Browser geöffnet, um die Einrichtung abzuschließen.

2. **Verwendung von Portainer:**
   - Nach dem Anlegen eines Admin-Kontos und der Auswahl von `Local` als Docker-Umgebung wurde die Übersicht aller laufenden Container angezeigt.
   - Portainer erleichtert die Überwachung der Containerleistung, indem es CPU- und Speichernutzung anzeigt, sowie Zugriff auf Logs und Metriken bietet.
   - Über die Web-Oberfläche konnten Container einfach gestartet, gestoppt und verwaltet werden.

   ![portainer](images/PortainerPicture.png)

Portainer stellte sich als wertvolle Ergänzung des Projekts heraus, da es eine klare Übersicht über die Container bot und Verwaltungsaufgaben vereinfachte.

### **Sicherheit im Container-Umfeld**
Aufgrund des statischen Charakters des Blogs ist eine lokale Kopie der Daten vorhanden, um die Webseite in einer neuen Umgebung wiederherstellen zu können.

## **Fertige Website**

Hier habe ich noch als Beweis die fertige Website, abgeändert nur zum SPASS ;)

![portainer](images/wordpress-website.png)

## **Fazit**
Dieses Projekt ermöglichte mir wertvolle Einblicke in Docker, eine Technologie, mit der ich bisher nicht vertraut war. Die Kombination von WordPress und MariaDB demonstrierte eindrucksvoll die Fähigkeiten von Docker bei der Verwaltung komplexer Anwendungen und bei der Verbesserung der Skalierbarkeit und Effizienz.

Besonders herausfordernd war das Monitoring meiner Docker-Container. Verschiedene Tools erwiesen sich als schwer zu konfigurieren, und oft waren die Anleitungen verwirrend oder unvollständig. Insbesondere PRTG erwies sich als komplex und instabil, was zu zusätzlichen Frustrationen führte. Auch Checkmk erfüllte meine Anforderungen nicht. 

Trotz dieser Schwierigkeiten konnte ich feststellen, dass Docker eine effektive Lösung für Tests und die Bereitstellung von Anwendungen ist. Es bietet eine flexible und leistungsfähige Plattform, die für viele Entwicklungs- und Betriebsaufgaben geeignet ist.

## **Quellenverzeichnis**
- [Informationen zu Containern](https://www.ibm.com/de-de/cloud/learn/containers)
- [Microservices AWS](https://aws.amazon.com/de/microservices/)
- [Microservices IBM](https://www.ibm.com/topics/microservices)
- [Synchrone & Asynchrone Services](https://learn.microsoft.com/de-de/dotnet/architecture/microservices/architect-microservice-container-applications/asynchronous-message-based-communication)
- [Sicherheit Container](https://www.vmware.com/de/topics/glossary/content/container-security.html)
- [Hilfestellung Setup MariaDB](https://mariadb.com/kb/en/installing-and-using-mariadb-via-docker/)
