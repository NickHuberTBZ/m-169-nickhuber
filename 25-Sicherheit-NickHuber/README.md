# M169 - 25 Infrastruktur Sicherheit

Diese Seite zeigt warum die Sicherheit von einer Infrastruktur ist in unserem Buisness besonders wichtig ist.

* [Infrastruktursicherheit](#Infrastruktursicherheit)
* [Firewall & Reverse-Proxy](#Firewall & Reverse-Proxy)
* [Reverse-Proxy](#Reverse-Proxy)
* [Benutzer- und Rechteverwaltung](#Benutzer- und Rechteverwaltung)
* [Konzepte](#Konzepte)
* [Prinzip des minimalen Benutzerprivilegs](#Prinzip des minimalen Benutzerprivilegs)
* [RBAC](#RBAC)
* [Lebenszyklusverwaltung von Benutzern](#Lebenszyklusverwaltung von Benutzern)
* [SSH](#SSH)
* [Zweck](#Zweck)

## Infrastruktursicherheit
Besonders in einer Produktionsumgebung ist die Sicherheit der Infrastruktur eines der wichtigsten Aspekte. Sie dient dazu, Integrität, Verfügbarkeit und den Schutz von Benutzerdaten zu gewährleisten. Im Folgenden werde ich die wesentlichen Schlüsselpunkte erläutern:

## Firewall & Reverse-Proxy
Firewalls und Reverse-Proxys sind wesentliche Bestandteile zur Sicherung Ihres Internetverkehrs und Ihrer Infrastruktur vor externen Eingriffen. Hier werde ich erklären, was sie sind und was sie tun.

### Firewall
Eine Firewall ist eine Software oder Hardware, die Pakete auf bösartige Inhalte überprüft. Anfangs konnten Firewalls nur bis zur OSI-Schicht 4 überprüfen, was bedeutet, dass sie nur auf der Transportschicht arbeiten konnten. Mit sogenannten Next-Generation-Firewalls (NGFWs) können Firewalls jedoch in den Schichten 3, 4, 5, 6 und 7 arbeiten. Dies bedeutet, dass NGFWs je nach den verfügbaren physischen Ressourcen auf fünf Ebenen parallel arbeiten können. Eine Firewall ist nicht nur eine Software; es handelt sich auch um ein physisches Netzwerkgerät, das zwischen dem WAN und LAN platziert werden kann, um den Internetverkehr zu überwachen und zu regulieren.

## Reverse-Proxy
Ein Reverse-Proxy fungiert als Vermittler. Er leitet den Verkehr von der Quelle Any/0 zu einer bestimmten Adresse innerhalb eines geschützten Netzwerks weiter. Zum Beispiel, wenn Sie einen Webserver in einer DMZ haben und auf einen DB-Server in einem privaten Subnetz zugreifen möchten, der nicht direkt von außen erreichbar ist, platzieren Sie einen Reverse-Proxy zwischen ihnen (logisch gesehen). Konfigurieren Sie den Reverse-Proxy so, dass er auf die Zieladresse 192.168.0.0/32:[Port] zeigt.

## Benutzer- und Rechteverwaltung
Die Benutzer- und Rechteverwaltung erfolgt häufig über ein zentrales IAM-System wie AWS IAM oder AD DS. Sie dient dazu, den Zugriff auf bestimmte Dateien, Verzeichnisse und physische Standorte einzuschränken.

## Konzepte
Die Benutzer- und Rechteverwaltung ist ein großes Fachgebiet, daher haben Menschen eine Faustregel erstellt. Ich werde sie hier kurz erklären:

## Prinzip des minimalen Benutzerprivilegs
Benutzern sollten minimalste Berechtigungen für alles zugewiesen werden, nur soweit, wie es die Arbeitsproduktivität nicht wesentlich beeinträchtigt. Das bedeutet, vermeiden Sie es, Benutzern übermäßige oder zusätzliche Privilegien zu geben. Wenn es wirklich erforderlich ist, geben Sie ihnen nur vorübergehende Berechtigungen.

## RBAC
RBAC ist ein Prinzip, das auch als `Role Based Access Control´ bezeichnet wird. Es gibt einem Benutzer/Mitarbeiter den entsprechenden Zugriff, der seiner Rolle im Unternehmen entspricht. Um das zu verdeutlichen, hier ein Beispiel: IAM (Identity Access Management) hat Jeremy die volle Kontrolle über sein Laufwerk gegeben und erlaubt ihm nur den Zugriff auf die erforderlichen Tools, mit denen er umgehen kann. Jeremy arbeitet in der Personalabteilung und hat nur Zugriff auf Zeitverwaltungstools und die Mitarbeiterdatenbank, die die persönlichen Informationen der Mitarbeiter enthält, sowie Jira für seine Tickets.

## Lebenszyklusverwaltung von Benutzern
Die Lebenszyklusverwaltung von Benutzern umfasst die Bereitstellung neuer Mitarbeiter/Benutzer mit einer Einführung in die Compliance-Richtlinien und den grundlegenden Verhaltenskodex. Während dieses Prozesses werden sie darauf hingewiesen, wie sie ihre Tools effektiv nutzen können, um ihre Aufgaben zu erfüllen.

Ähnlich verläuft der Offboarding-Prozess, wenn Benutzer/Mitarbeiter das Ökosystem/Unternehmen verlassen. In diesem Prozess werden die Daten des Benutzers/ Mitarbeiters aus dem mit seinem Konto verknüpften Speicher gelöscht, und seine Zugriffsrechte werden widerrufen. Gemäß der Unternehmensrichtlinie können personenbezogene Mitarbeiterdaten bis zu 10 Jahre lang archiviert werden. Nach dieser Frist können die Mitarbeiterdaten dauerhaft gelöscht werden.

Die Lebenszyklusverwaltung von Benutzern umfasst auch Änderungen, die Benutzer/Mitarbeiter während ihrer Tätigkeit im Unternehmen durchlaufen. Wenn zum Beispiel Pascal zunächst in der Verwaltung arbeitet und dann zum Leiter der Kundenkommunikationseinheit des Unternehmens wechselt, erlebt er eine Rollenänderung. In solchen Fällen werden Anpassungen an Berechtigungen vorgenommen, die zusätzlichen Zugriff gewähren oder frühere Berechtigungen einschränken, wie es erforderlich ist.

## SSH
SSH ist eine weit verbreitete Remote-Zugriffstechnologie. Hier werde ich es im Detail erklären.

## Zweck
Secure Shell (SSH) steht als Eckpfeiler der Netzwerksicherheit und dient als Kommunikationsprotokoll, das für die Verwaltung und den Zugriff auf Netzwerkgeräte über unsichere Netzwerke entwickelt wurde. Es gewährleistet einen geschützten Pfad für sensible Interaktionen, der in einer Ära, in der die Datensicherheit oberste Priorität hat, unerlässlich ist. Die primäre Rolle von SSH besteht darin, den Fernzugriff auf Server und Systeme zu schützen und einen höheren Schutz als herkömmliche passwortbasierte Anmeldungen zu bieten.
