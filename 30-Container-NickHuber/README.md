
# M169 - 30 Container

### Inhaltsverzeichniss

- 01 - Container
- 02 - Docker
- 03 - Netzwerk-Anbindung
- 04 - Volumes
- 05 - Image-Bereitstellung


## 01 Container

**Beschreibung**
Container sind resourcensparende, eigenständige Laufzeitumgebungen, in welchen Anwendungen in verschiedenen Umgebungen konsistent und isoliert ausgeführt werden könen, unabhängig von der zugrundeliegenden oder physischen Infrastruktur.

**Merkmale**

- Container teilen sich Ressourcen mit der Host-Betriebssystem.
- Container können im Bruchteil einer Sekunde gestartet und gestoppt werden
- Anwendungen, die in Containern laufen, verursachen wenig bis gar keinen Overhead
- Container sind portierbar --> Fertig mit "Aber bei mir auf dem Rechner lief es doch!"
- Container sind leichtgewichtig, d.h. es können dutzende parallel betrieben werden.
- Container sind "Cloud-ready"!

### Microservices

**Beschreibung**

Microservices sind ein weg Softwaresysteme so zu entwickeln und zu kombinieren, dass sie aus unabhängigen Komponenten bestehen, welche isoliert untereinander in einem Netz interagieren.
Ein Monolith ist der "ältere/klassische" weg der software entwicklung, bei welcher alle prozesse in einen grossen Klotz zusammengefügt werden und nicht unabhängig voneinander laufen können

Ein grosser Vorteil von microservices ist, dass resourcen für verschiedene Services skalierbar sind, ohne dass das gesamte system verändert wird.

## 02 Docker

### Docker Daemon

- Erstellen, Ausführen und Überwachen der Container
- Bauen und Speichern von Images
- Der Docker Daemon wird normalerweise durch das Host-Betriebssystem gestartet.

### Docker Client

- Bedienung von Docker über die Kommandozeile (CLI) mittels des Docker Clients
- Kommuniziert per HTTP REST mit dem Docker Daemon

### Images

- Gebuildete Umgebungen, die als Container gestartet werden können
- Images sind nicht veränderbar, sondern können nur neu gebuildet werden.
- Bestehen aus Namen und Version (TAG), z.B. ubuntu:16.04.

### Container

- Ausgeführte Images
- Können beliebig oft als Container ausgeführt werden
- Können verändert werden; dazu werden sogenannte Union File Systems verwendet.

### Docker Registry

- Speichert und verteilt Images
- Standard-Registry ist der Docker Hub
- Viele Organisationen und Firmen nutzen eigene Registries

## Befehle

#### docker run

- Starten neuer Container
- Unterstützt viele Argumente für Konfiguration

#### docker ps

- Gibt Überblick über aktuelle Container

#### docker images

- Gibt Liste lokaler Images aus

#### docker rm und docker rmi

- Löschen von Containern und Images

#### docker start

- Startet gestoppte Container

### Informationen zu Containern

- Logs, Inspektion, Dateisystem-Änderungen, Prozessinformationen

### Dockerfile

- Textdatei mit Schritten zum Erzeugen eines Docker-Images

### Konzepte

#### Build Context

- Erforderlich für den Befehl `docker build`

#### Layer / Imageschichten

- Jede Anweisung in einem Dockerfile führt zu einer neuen Imageschicht

#### Anweisungen im Dockerfile

- FROM, ADD, CMD, COPY, ENTRYPOINT, ENV, EXPOSE, HEALTHCHECK, MAINTAINER, RUN, SHELL, USER, VOLUME, WORKDIR

Es scheint, als ob Sie Ihre Fortschritte und Notizen bezüglich der Arbeit mit Docker zusammenfassen möchten. Hier ist eine überarbeitete Version, die die von Ihnen angegebenen Informationen enthält:

## 03 Netzwerk Anbindung

Um einen Webserver, der in einem Container läuft, für die Öffentlichkeit zugänglich zu machen, können Ports mit den Befehlen `-p` und `-P` veröffentlicht werden. Dies leitet Ports auf den Host des Containers weiter.

Hier sind einige Beispiele aus dem Modul Repository:
[Beispiele aus dem Modul Repository](https://gitlab.com/florian.froebel/modul-169/-/tree/main/Tag_4/Unterlagen%20aus%20Modul%20Repo?ref_type=heads#beispiele-aus-dem-modul-repository)

Darüber hinaus müssen noch ein paar Schritte unternommen werden, damit der Host auf den Container zugreifen kann:
[Von Host auf Container zugreifen](https://gitlab.com/florian.froebel/modul-169/-/tree/main/Tag_4/Unterlagen%20aus%20Modul%20Repo?ref_type=heads#von-host-auf-container-zugreifen)

### Container Networking

Bei Docker können "Netzwerke" getrennt von Containern erstellt und verwaltet werden. Container lassen sich einem bestehenden Netzwerk zuweisen, sodass sie sich direkt mit anderen Containern im gleichen Netzwerk austauschen können.

Standardmäßig werden folgende Netzwerke eingerichtet:

- bridge: Das Standard-Netzwerk, in dem gemappte Ports nach außen sichtbar sind.
- none: Für Container ohne Netzwerkschnittstelle bzw. ohne Netzverbindung.
- host: Fügt den Containern dem internen Host-Netzwerk hinzu; Ports sind nicht nach außen sichtbar.

Hier sind die Befehle für das Container Networking:
[Befehle Container Networking](https://gitlab.com/florian.froebel/modul-169/-/tree/main/Tag_4/Unterlagen%20aus%20Modul%20Repo?ref_type=heads#befehle-container-networking)

## 04 Volumes

### Arten von Persistenter Speicherung in Docker

Um Daten persistent zu halten, bietet Docker verschiedene Möglichkeiten:

- Ablegen der Daten auf dem Host
- Teilen der Daten zwischen Containern
- Eigene Volumes erstellen, um Daten abzulegen

#### Kurze Definition Volumes

Volumes sind Speichereinheiten, die in Container "gemounted" werden können. Sie können Konfigurationsdateien, Rohdaten usw. enthalten.

Volumes bieten mehrere nützliche Funktionen für persistente oder gemeinsam genutzte Daten, wie z.B.:

- Direkte Initialisierung beim Erstellen eines Containers
- Möglichkeit, dass ein Container mehrere Volumes verwendet
- Unabhängigkeit vom Lebenszyklus des Containers

#### Named Volumes

Seit Docker Version 1.9 können Volumes mit dem Befehl `docker volume` verwaltet werden. Dadurch können verschiedene Volume-Driver-Dateisysteme für Container bereitgestellt werden, und ein Volume kann mehreren Containern bereitgestellt werden.

Beispiele und Befehle für Named Volumes sind ebenfalls vorhanden.

### Eigenen Dockercontainer Starten

#### Image auswählen

Für Testzwecke wird das Image httpd ausgewählt, um später mit einem Webserver zu experimentieren.

#### Befehle für Docker

1. `docker pull httpd`
2. `docker run -it -p 80:80 --name Test1 httpd`
3. `docker exec -it Test1 bash/sh`
4. `docker run -it -p 80:80 --volume /var/www/html:/home/user/lokal --name Test1 httpd`

## 05 Image-Bereitstellung

Nachdem Sie Ihren Dockercontainer erstellt und konfiguriert haben, möchten Sie möglicherweise Ihr Image bereitstellen, damit es von anderen verwendet werden kann. Hier sind einige Schritte, um Ihr Docker-Image zu veröffentlichen:

### Docker Hub

Der Docker Hub ist ein öffentliches Repository für Docker-Images. Sie können Ihr Image hier hochladen und mit anderen teilen.

1. **Registrieren oder Anmelden:** Falls Sie noch keinen Account haben, registrieren Sie sich auf [Docker Hub](https://hub.docker.com/). Andernfalls melden Sie sich an.

2. **Taggen Sie Ihr Image:** Bevor Sie Ihr Image hochladen können, müssen Sie es taggen, um es mit Ihrem Docker Hub Benutzernamen zu versehen. Verwenden Sie dazu den Befehl:
   ```bash
   docker tag your_image your_username/your_image
   ```
   Ersetzen Sie `your_image` durch den Namen Ihres Images und `your_username` durch Ihren Docker Hub Benutzernamen.

3. **Hochladen des Images:** Verwenden Sie den Befehl `docker push`, um Ihr Image hochzuladen:
   ```bash
   docker push your_username/your_image
   ```
   Dadurch wird Ihr Image in Ihrem Docker Hub Repository veröffentlicht.

### Verwenden Sie ein anderes Registry

Sie können auch andere Docker-Registries verwenden, wenn Sie Ihr Image nicht auf dem Docker Hub veröffentlichen möchten. Einige Unternehmen betreiben interne Docker-Registries für die interne Verwendung.

### Private Repositories

Wenn Sie Ihr Image privat halten möchten, können Sie ein privates Repository auf dem Docker Hub erstellen oder ein anderes Registry verwenden, das private Repositories unterstützt.

Nachdem Sie Ihr Image veröffentlicht haben, können andere Benutzer es mit dem Befehl `docker pull` herunterladen und verwenden.

Das sind die grundlegenden Schritte, um Ihr Docker-Image für die Veröffentlichung vorzubereiten. Sie können auch zusätzliche Schritte wie das Hinzufügen von Dokumentation, das Hinzufügen von Tags für verschiedene Versionen usw. durchführen, um Ihr Image für andere Benutzer ansprechender zu machen.

**Verwendete Resourcen:**

- https://gitlab.com/ch-tbz-it/Stud/m169/-/tree/main/30-Container?ref_type=heads 
- https://symbl.cc/de/
