## 35 Containersicherheit

Um eine umfassende und sichere Docker-Umgebung zu gewährleisten, ist es wichtig, die Sicherheitsaspekte der Container und deren Betriebsumgebung zu berücksichtigen. Dieses Kapitel unterteilt sich in drei Hauptbereiche: Protokollieren & Überwachen, Container sichern & beschränken und Kontinuierliche Integration.

### 01 Protokollieren und Überwachen

#### Logging

Das Protokollieren von Ereignissen in Docker-Containern ist entscheidend für die Fehlersuche und das Monitoring. Standardmäßig werden alle Ausgaben an STDOUT und STDERR protokolliert. Verschiedene Logging-Methoden können über den `--log-driver`-Parameter gesteuert werden, wie z.B. das Standard-Logging im JSON-Format oder das Logging über das System-Log (Syslog) des Hosts.

#### Überwachen und Benachrichtigen

Die Überwachung von Docker-Containern ist wichtig, um die Systemintegrität aufrechtzuerhalten und potenzielle Probleme frühzeitig zu erkennen. Tools wie cAdvisor bieten einen Überblick über Ressourcennutzung und Performance-Metriken von Containern.

### 02 Container sichern und beschränken

Um Docker-Container sicher zu betreiben, müssen verschiedene Sicherheitsaspekte berücksichtigt werden:

- Kernel-Exploits und Denial-of-Service-Angriffe verhindern.
- Container-Breakouts und manipulierte Images erkennen und verhindern.
- Sensible Daten in Containern schützen und den Zugriff auf vertrauliche Informationen kontrollieren.
- Das Prinzip des Least Privilege anwenden, um die Zugriffsrechte von Containern zu beschränken.
- Zugriffe auf das Host-System und andere Container begrenzen, um potenzielle Sicherheitslücken zu minimieren.

Zusätzliche Sicherheitsmaßnahmen umfassen das Setzen von Benutzerrechten, die Begrenzung von Ressourcen (Speicher, CPU), das Einschränken von Dateisystemzugriffen und das Anwenden von Sicherheitsprofilen wie AppArmor oder SELinux.

### 03 Kontinuierliche Integration / Automatisierung

Kontinuierliche Integration (CI) ist ein wesentlicher Bestandteil eines sicheren und effizienten Entwicklungsprozesses. Sie umfasst Schritte wie gemeinsame Codebasis, automatisierte Builds und Tests, Integration in den Hauptbranch und regelmäßige Berichterstattung über den Entwicklungsfortschritt.

Tools wie TravisCI, Jenkins und Blue Ocean bieten Funktionen zur Automatisierung von CI/CD-Pipelines und ermöglichen Entwicklern eine schnelle und effiziente Entwicklung und Bereitstellung von Anwendungen in Docker-Containern.

Durch die Beachtung dieser Sicherheitspraktiken und die Verwendung geeigneter Tools können Docker-Container sicher betrieben und entwickelt werden, ohne die Integrität des Systems zu gefährden.