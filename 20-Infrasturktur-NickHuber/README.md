# M169 - 20 Infrastruktur Automatisierung

Durch die Nutzung von Infrastructure as Code und Cloud-Services können Sie Ihre IT-Infrastruktur effizient automatisieren, was Ihnen Zeit spart, Skalierbarkeit ermöglicht und für konsistente Umgebungen in der Softwareentwicklung und Systemverwaltung sorgt.

## Inhaltsverzeichnis

* [Cloud Computing](#cloud-computing)
* [Infrastructure as Code](#infrastructure-as-code)
* [Mögliche Serverdienste für die Automatisierung](https://wiki.ubuntuusers.de/Serverdienste/)
* [Wissenswert](#wissenswert)

## Cloud Computing
[Video: Cloud Computing Services Models - IaaS PaaS SaaS Explained](https://www.youtube.com/watch?v=36zducUX16w)

Cloud Computing bezieht sich auf die Bereitstellung von IT-Ressourcen über das Internet. Es umfasst verschiedene Service-Modelle wie Infrastructure as a Service (IaaS), Platform as a Service (PaaS) und Software as a Service (SaaS). Diese Modelle ermöglichen es Unternehmen, Rechenleistung, Speicher und Anwendungen bedarfsgerecht zu nutzen, ohne physische Hardware vor Ort betreiben zu müssen.

## Infrastructure as Code
Infrastructure as Code (IaC) ist ein Ansatz zur Automatisierung der Bereitstellung und Verwaltung von IT-Infrastruktur durch die Verwendung von Code. Durch die Definition von Infrastruktur in Dateien können Entwickler und Administratoren Prozesse automatisieren, wiederholbar machen und leicht skalieren. Beliebte Tools für IaC sind Terraform, Ansible und Chef.


## Mögliche Serverdienste für die Automatisierung
Eine Liste von Serverdiensten, die für die Automatisierung von Infrastruktur genutzt werden können. Diese Dienste umfassen verschiedene Aspekte wie Webserver, Datenbanken, Virtualisierung und Netzwerkdienste, die für die Entwicklung und den Betrieb von Anwendungen benötigt werden.

## Wissenswert
Unter **[Cloud Computing](https://de.wikipedia.org/wiki/Cloud_Computing)** versteht man die Ausführung von Programmen, die nicht auf dem lokalen Rechner installiert sind, sondern auf einem anderen Rechner, der aus der Ferne aufgerufen wird (bspw. über das Internet).

Eine **dynamische Infrastruktur-Plattform** ist ein System, das Rechen-Ressourcen bereitstellt (Virtualisiert),
insbesondere Server (compute), Speicher (storage) und Netzwerke (networks), und diese
Programmgesteuert zuweist und verwaltet, sogenannte **Virtuelle Maschinen** (VM).

Damit "Infrastructure as Code" auf "Dynamic Infrastructure Platforms" genutzt werden können, müssen sie die folgenden Anforderungen erfüllen:

- **Programmierbar** - Ein Userinterface ist zwar angenehm und viele Cloud Anbieter haben eines, aber für "Infrastructure as Code"
muss die Plattform via Programmierschnittstelle ([API](https://de.wikipedia.org/wiki/Programmierschnittstelle)) ansprechbar sein.
- **On-demand** - Ressourcen (Server, Speicher, Netzwerke) schnell erstellen und vernichtet.
- **Self-Service** - Ressourcen anpassen und auf eigene Bedürfnisse zuschneiden.
- **Portabel** - Anbieter von Ressourcen müssen austauschbar sein.
- Sicherheit, Zertifizierungen (z.B. [ISO 27001](https://de.wikipedia.org/wiki/ISO/IEC_27001)), ...
