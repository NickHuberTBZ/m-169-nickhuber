# 7. Woche Modul 169

## Was habe ich gemacht?
Ich habe heute den Teil 35 beendet und dann mit der Planung weiter gemacht.

## Wo hatte ich Probleme?
Bei nichts grossem.

## Was mache ich das nächste Mal?

Planung fertig zu schreiben und danach möglichst bald mit der Umsetzung des Projektes zu beginnen.

## Ergebnisse

 - **[35 - Container-Sicherheit](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/35-Container-Sicherheit)**