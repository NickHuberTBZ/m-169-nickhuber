# 2. Woche Modul 169

## Was habe ich gemacht?

Diese Woche war Herr Rohr während der Lektion krank deswegen konnten wir nach Hause arbeiten gehen, dort habe ich die Aufgaben von 10 fertig gestellt, da ich diese letzte Woche nicht ganz fertig geschafft habe. 

Danach habe ich 20 durchgearbeitet und dies ein bisschen Dokumentiert.

## Wo hatte ich Probleme

Ich hatte kurzzeitig Probleme mit den Extensions von Visual Studio, ich habe dies aber schnell lösen können. Weil ich noch eine alte Version von Visual Studio installiert habe.

## Was mache ich das nächste Mal?

Das nächste Mal werde ich noch den kleinen Teil von 25 fertig "lernen".

## Ergebnisse

 - **[10 - ToolUmgebung aufsetzen](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/10-ToolUmgebung-NickHuber)**
 - **[20 - Infrastruktur automatisieren](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/20-Infrastruktur-NickHuber)**
 
