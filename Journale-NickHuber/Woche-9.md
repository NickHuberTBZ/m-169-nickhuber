### **Woche 9: Modul 169**

## **Was habe ich gemacht?**
- Eine `docker-compose.yml`-Datei mit den Services für die MySQL-Datenbank und WordPress erstellt.
- Den MySQL-Container mit den erforderlichen Umgebungsvariablen für die Datenbank gestartet.
- Den WordPress-Container konfiguriert und gestartet.
- WordPress mit den Datenbankverbindungsdetails eingerichtet.

## **Wo hatte ich Probleme?**
- Probleme bei der Verknüpfung von WordPress mit der Datenbank aufgrund falscher Umgebungsvariablen.
- Schwierigkeiten bei der ordnungsgemäßen Konfiguration des WordPress-Containers, was zu Fehlermeldungen führte.

## **Was mache ich das nächste Mal?**
- Die Umgebungsvariablen sorgfältiger prüfen, um Konfigurationsfehler zu vermeiden.
- Die WordPress-Konfiguration vor dem Start des Containers gründlich überprüfen.

## **Ergebnisse**
Zu meinen Projekt Ergebnissen geht es [hier!](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/AbschlussProjekt)