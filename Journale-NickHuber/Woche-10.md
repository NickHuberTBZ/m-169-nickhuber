
### **Woche 10: Modul 169**

## **Was habe ich gemacht?**
- Portainer als Monitoring-Tool eingerichtet.
  - Docker-Volume für Portainer erstellt.
  - Portainer-Container gestartet.
  - Portainer im Browser konfiguriert und eingerichtet.
- Die WordPress-Website auf ihre Funktionen getestet.
- Fehler in der WordPress-Konfiguration identifiziert und behoben.
- Die Docker-Compose-Datei optimiert.
- Eine umfassende Dokumentation über das Projekt erstellt.

## **Wo hatte ich Probleme?**
- Schwierigkeiten beim Zugriff auf Portainer aufgrund von Firewall-Einstellungen.
- Einige WordPress-Funktionen haben nicht korrekt funktioniert, was zu Fehlermeldungen führte.

## **Was mache ich das nächste Mal?**
- Abgabe des Projekts

## **Ergebnisse**
Zu meinen Projekt Ergebnissen geht es [hier!](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/AbschlussProjekt)