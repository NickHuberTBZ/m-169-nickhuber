# 3. Woche Modul 169

## Was habe ich gemacht?
Heute habe das Kapitel 25 bearbeitet, dies ging um Sicherheit in diesen umgebungen. Das Ziel des Kapitels war es die Sicherheit einer solchen Umgebung näher zu bringen und sich einige Beispiele machen und vorstellen. Ich habe dann noch im Internet nach Beispielen gesucht, um mir ein Bild davon zu machen.

Danach habe ich das Kapitel 30 - Container angefangen, dort bin ich bis Nummer 1 gekommen.

## Wo hatte ich Probleme?
Bei nichts grossem.

## Was mache ich das nächste Mal?

Das nächste Mal werde ich am Kapitel 30 weiter machen und dies hoffentlich beenden.

## Ergebnisse

 - **[25 - Sicherheit](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/25-Sicherheit-NickHuber)**
 - **[30 - Container](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/30-Container-NickHuber)**