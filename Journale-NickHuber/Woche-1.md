# 1. Woche Modul 169

## Was habe ich gemacht?
Nachdem ich die Aufgaben zur Erstellung des SSH-Keys und zur Konfiguration des Git-Clients abgeschlossen habe, fühle ich mich sicherer und besser vorbereitet, um effektiv mit Gitlab zu arbeiten. Die Einrichtung des SSH-Keys ermöglicht eine sichere Verbindung zum Gitlab-Server, während die Konfiguration des Git-Clients mir die nötigen Werkzeuge für die Zusammenarbeit an Projekten bereitstellt. Besonders hilfreich war die klare Anleitung zum Klonen, Pullen und Pushen von Repositories, was mir ein reibungsloses Arbeiten ermöglicht. Insgesamt habe ich ein tieferes Verständnis für die Nutzung von SSH-Keys und Gitlab gewonnen und fühle mich bereit, meine Projekte effizient zu verwalten und zu teilen.

## Wo hatte ich Probleme?
Ich hatte am Anfang Probleme mit der Erstellung meiner SHH Keys, dies hat auch nicht funktioniert. Ich habe dann einfach meine alten SHH Keys verwendet. 

## Was mache ich das nächste Mal?

Das nächste Mal muss ich noch an dieser Aufgabe weiter arbeiten, da ich nicht ganz fertig geworden bin. Ich bin nur bis Punkt 3 gekommen, diesen habe ich nur durchgelesen.

## Ergebnisse

 - **[10 - ToolUmgebung aufsetzen](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/10-ToolUmgebung-NickHuber)**
