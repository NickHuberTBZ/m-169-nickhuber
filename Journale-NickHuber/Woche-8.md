### **Woche 8: Modul 169**

## **Was habe ich gemacht?**
- Die Anforderungen für das Projekt definiert und die Projektplanung erstellt.
- Die notwendigen Komponenten für das Projekt identifiziert.
- Docker Desktop von der offiziellen Website heruntergeladen und installiert.
- Docker Desktop auf Windows mit WSL2-Unterstützung eingerichtet.

## **Wo hatte ich Probleme?**
- Bei der Einrichtung von Docker Desktop gab es anfängliche Probleme mit der WSL2-Konfiguration.
- Es gab Schwierigkeiten bei der Installation von Docker Desktop aufgrund von Kompatibilitätsproblemen mit dem vorhandenen Betriebssystem.

## **Was mache ich das nächste Mal?**
- Sicherstellen, dass das Betriebssystem die Mindestanforderungen erfüllt, bevor Docker Desktop installiert wird.
- Detaillierte Anleitungen zur Installation und Einrichtung von Docker Desktop studieren.

## **Ergebnisse**
Zu meinen Projekt Ergebnissen geht es [hier!](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/AbschlussProjekt)
