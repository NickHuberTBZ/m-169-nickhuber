# M-169-NickHuber

- [**Journale M-169**](/Journale-NickHuber)

- [**Abschlussprojekt**](/Abschlussprojekt)

- **Theorie**
    - [10 - ToolUmgebung](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/10-ToolUmgebung-NickHuber)
    - [20 - Infrasturktur](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/20-Infrasturktur-NickHuber)
    - [25 - Sicherheit](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/25-Sicherheit-NickHuber)
    - [30 - Container](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/30-Container-NickHuber)
    - [35 - Container Sicherheit](https://gitlab.com/NickHuberTBZ/m-169-nickhuber/-/tree/main/35-Container-Sicherheit)